# Cloud Technologies
#Облачные технологии и услуги#

Участники группы: Хисаметдинова Динара (https://gitlab.com/khdinova), Филиппов Артём (https://gitlab.com/superartem), Беломытцев Андрей, Фадеев Дмитрий (https://gitlab.com/Ayfilor)

Аналитическая лабораторная по облакам №1 (Azure): [Лаб_1](https://gitlab.com/meow9012838/cloud-technologies/-/blob/main/Analytics_Cloud_lab1.md?ref_type=heads)

Аналитическая лабораторная по облакам №2 (AWS): [Лаб_2](https://gitlab.com/meow9012838/cloud-technologies/-/blob/main/Analytics_Cloud_lab2.md?ref_type=heads)

Devops [лабораторная №1](https://gitlab.com/meow9012838/cloud-technologies/-/tree/main/devops/lab1?ref_type=heads)

Devops [лабораторная №2](https://gitlab.com/meow9012838/cloud-technologies/-/tree/main/devops/lab2?ref_type=heads)

Devops [лабораторная №3](https://gitlab.com/meow9012838/cloud-technologies/-/tree/main/devops/lab3?ref_type=heads)

[Экзамен](https://gitlab.com/meow9012838/cloud-technologies/-/blob/main/Exam.md)
