# Аналитическая лабораторная работа по облачным технологиям
## __Состав команды выполнивших:__ Хисаметдинова Динара, Филиппов Артём, Беломытцев Андрей, Фадеев Дмитрий

*Цель работы:* узнать о назначении различных облачных сервисов, проанализировать фичи, то, за что проходит оплата каждого из них, понять, к какому уровню абстракции относится каждый, исходя из этих знаний найти аналоги среди российских сервисов.

*Дано:* 
1. Слепок данных биллинга от провайдера
2. Google с документациями провайдера

*Процесс*

---

Сепок данных биллинга провайдера был проанализирован, после чего была изучена документация AWS как общая, так и специфическая по каждому сервису, включая billing plans (https://docs.aws.amazon.com/), затем изучен маппинг сервисов Amazon на аналогичные в Yandex Cloud (https://cloud.yandex.ru/docs/overview/platform-comparison/aws), список сервисов в которых скуден, но был полезен для расписывания нескольких сервисов, таких, как Amazon Elastic Compute Cloud и Amazon Route 53. У VK Cloud нет такого маппинга, однако интерфейс ну очень доступный, и, благодаря опыту прошлой лабораторной с сервисами Azure, я помнила назначения и названия, поэтому быстро организовала соответствие вкшных а амазон сервисов. В некоторых случаях, у сервисов Azure более широкий функционал, чем у российских аналогов (и наоборот), поэтому иногода писала service name1 + service name2, чтобы наиболее точно и без потерь импортозаместить инструменты. Хочу отметить то, что документация у Azure весьма разрозненная и не совсем интуитисно понятная, чего не скажешь об AWS. На удобная изображении навигационная панель, а также инструмент поиска не только по названиям сервисов, но и по их назначению.
![Шапка](Panel.png)
![Инструмент поиска](Bar.png)
![Таблица](Table1.png)
![Таблица](Table2.png)

Как и в прошлой работе, для заполнения таблицы, сначала все сервисы были расположены в порядке IT Tower от сетевого уровня к уровню приложений {Networking, Infrastructure, Security, Cloud Service, Compute, Data, Application}. Затем заполнила семейство сервиса, категорию и подкатегорию сервиса, полное название сервиса, то, за что будут списываться средства, код (название) конкретного продукта, единицы измерения использования этого продукта, конкретная операция, за которое взимается плата ([lineItem/Operation]), детальное описание этой операции, например, 'Sign up charge for subscription:%' - т.е. плата за подписку, и, наконец, столбцы с аналогичными сервисами на российском рынке (Yandex, VK).

## ***Описание сервисов Azure и российских аналогов (Yandex Cloud и VK Cloud)*** ##

-----
__Amazon EC2__ представляет собой безопасную платформу для облачных вычислений с низкой задержкой благодаря глобальному присутствию, в которой немного менее тысячи инстансов, поддерживающая все основные архитектуры процессоров (включая ARM) и является единственным облаком на рынке с сетью Ethernet 400 Гбит/с. Основные Selling points - максимальная гибкость, огромное количество тарифных планов в зависимости от рабочей нагрузки, потребностей и бюджетных ограничений (например, Memory Optimized подходит для приложений, требующих использование больших объёмов памяти, например, машинное обучение, запуск Hadoop, Accelerated Computing - для параллельной обработки данных и т.д.). Экземпляр EC2 - это виртуальная машина, которая получает определенный процент оперативной памяти, процессора, дисков и сетевых входов/выходов базового физического хоста. В первой строке биллинга встретился такой тип использования, как %Box Usage% это посекундная(!) оплата за использование инстанса EC2. AMIs, т.е. Amazon Machine Instances изначально спроектированы под увеличивающийся список операционок, в том числе их собственную (Amazon Linux 2), Windows (что редкость в российских аналогах), и огромный спимок дистрибутивов Линукс.

__Yandex Compute Cloud__ также является сервисом облачных вычислений, является точным аналогом EC2, и, в отличие от VK, предоставляет большую вариативность физических процессоров, включая AMD EPYC. Тарификация так же посекундная. Система списывания денег за ВМ такая же, как у AWS и VK - за использование CPU/RAM деньги снимаются только при работающей машине, а за диски - вне зависимости от работы виртуалки.

__Cloud Servers__ Из доступных ОС есть российские дистрибутивы Linux (AlmaLinux, AstraLinux, Red OS, Альт Сервер), а также Ubuntu, Debian, CentOS и пару других, что делает решение привлекательным для государственных компаний. предлагают только процессоры Intel Xeon Gold, так же, как и в AWS есть посекундный биллинг (Pay-as-you-go) для оплаты исключительно того объёма услуг, которую расходуешь. Также есть готовые конфигурации тарифов: basic, standard и advanced

Ниже представлены скрины, отображающие максимальную схожесть по цене решений по виртуализации Yandex Compute Cloud и VK Cloud Servers при схожих конфигурациях.

![Cloud Servers](Cloud_Servers.png)
![Cloud Computing](Cloud_Computing.png)
----
__Compute Savings Plans__ отличный инструмент экономии за счёт того, что пользователь обязуется использовать определённый объём вычислительной мощности в течение одного/трёх лет. Схема такая же, как при оптовых закупках - снижение цены за счёт большого количества покупаемой продукции. Размер скидки может доходить до 72%. Ещё одно преимущество для получателя услуги - неизменность цены на протяжение всего периода использования, т.е. инфляция не увеличит цену. Подробнее об условиях написано в AWS Cost Explorer. Существует несколько планов, включая All upfront (требует полной предоплаты за весь срок), Partial upfront (нужна частичная предоплата) и No upfront (без предоплаты, но с меньшей скидкой).

__Reserved Consumption__ возможность пользователям получить скидку до 22% за заблаговпеменную бронь ресурсов от 6 месяцев до года. Прекрасный способ экономии, суть работы та же, что и в AWS.

----
__AmazonEC2RIResale2__ относится к Reserved Instance Marketplace - уникальной P2P службой для купли-продажи инстансов. Это позволяет продавцам получить деньги за неиспользованные инстансы, а покупателям приобрести их дешевле. Как посредник, Amazon продает экземпляр (если кто-либо продаёт инстанс с такими параметрами) с самой низкой стоимостью, пока весь заказ покупателя не выполнится, а потом передает право собственности на зарезервированные экземпляры получателю. Прямых аналогов, предлагаемыми Яндексом и ВК, нет. "%RIResaleServiceFee" представляет собой сервисный сбор, взимающийся при продаже зарезервированного экземпляра на маркетплейсе.

__Yandex Cloud Marketplace__ вообще сложно отнести к аналогам, ибо это не P2P, а B2C рынок, предоставляющий клиентам выбрать продукты среди разнообразных сторонних вендоров. В отличие от AmazonEC2RIResale2, там присутствуют не только инстансы, но и продукты из таких категорий, как БД, сетевая инфраструктура, VoIP, ОС, удалённые рабочие столы и т.д.

__VK Cloud Marketplace__ суть та же, что и у Яндекса, но маркетплейс меньше по охвату поставщиков. Сам ВК не просто бездействующий посредник, но и подготавливает экземпляр сервиса, управляет и тарифицирует его. В маркетплейс входят как опенсорс сервисы, так и предоставляемые дистрибьютором.

----
__AmazonCloudFront__ является content delivery network, которая, как я уже описывала в прошлой лабораторной, позволяет максимально быстро и безопасно доставить контент сайтов, приложений в любой регион, так как серверы (Points Of Presence) расположены по равномерно по миру. Трафик пользователя направляется через глобальную сеть CDN для ускорения загрузки статического и динамического контента. Пользовательские запросы перенаправляются к ближайшему edge location, где контент кэшируется для быстрой доставки. Если контент не находится на краю сети, он извлекается из исходного местоположения. CloudFront также интегрируется с AWS Shield для защиты от DDoS-атак. За счёт быстрой загрузки сайта он поднимется выше в поисковике, что увеличит охваты. Один из подходящих сценариев использования CDN -раздача мультимедиа. Например, среди клиентов есть SUPERCELL, поэтому можно с низким пингом играть в Brawl Stars даже в Африке.

__Yandex Cloud CDN__ предоставляет почти абсолютно аналогичный функционал, только сосредоточен больше на рынке Восточной Европы и России. Всего CDN-ов более 140. Также Яндекс предоставляет инструменты аналитики трафика и запросов. Также, как и в AWS присутствует защита от ддос атак. Цены демократичнее, чем у AWS.

__VK Content Delivery Network__ суть та же, что и в первых двух сервисах. Основной сервер разгружен, а CDN принимвют на себя весь трафик. Производитель заявляет среднюю задержку в 20 мс и более 400 ЦОДов на 5 континентах (Хотя в Южной Америке лишь 1).

----
__Amazon Kendra__ представляет собой поисковый сервис на основе AI, который воспринимает поисковые запросы в виде естественных вопросительных предложений и применяет алгоритмы глубокого обучения для выявления контекста. Она полностью самостоятельна, пользователям не нужны собственные серверы, а также разработка, обучение и применение отдельных ML-моделей. Служба оптимизирована под узкие сферы и хорошо 'понимает' специфическую терминологию, в том числе научную, даже во  всех презентационных материалах позиционируется как enterprise-поисковик. Kendra может подключаться к различным источникам данных для индексации и поиска информации - коннекторам. Они облегчают интеграцию с популярными хранилищами данных, что делает поисковые запросы актуальными. 

__YandexGPT API__ предлагает использовать разработанную командой Яндекса модель для бизнес-нужд, приложений, в общем, продвигается больше как B2B решение, хотя и стало популярным среди обычных пользователей. Он пока находится в бета-версии. Уже неплохо справляется с улучшением контента, лендингов, копирайтом/рерайтом. Бесплатен, но для ликвидации лимита на количество запросов нужно приобрести подписку, также стоимость рассчитывается по числу токенов в запросе, модели (есть YandexGPT Lite и YandexGPT2) и режиму (синхронный/асинхронный). Интересной фишкой является и то, что LLM можно дообучить, отправив JSON с запросами и ответами через DataSphere.

У VK Cloud из отдалённо похожих сервисов есть только __Cloud Vision__ для распознавания текста, документов, объектов на видео и фото. Также служит инструментом для автоматпзации человеческого труда и является B2B решением.

----
__Amazon Lex__ работает на том же движке, что и голосовой помощник Alexa, создаёт интерфейсы. Он использует понятия намерений (intent) для определения действий, которые необходимо выполнить, промптов для запроса данных от пользователя, слотов (slot) для сбора необходимых данных, и обработку (fulfillment) для выполнения намеренных действий. Благодаря этому Lex по сути повторяет пайплайн работы программиста-создателя чат-ботов, не только разрабатывая, подстраиваясь под нужды конкретного бота, но и тестируя, доводя до конечной версии (что пугает меня). Распознавание речи подходит под работу со звуком с частотой дискретизации 8 кГц, то есть с обычным смартфоном.

Прямых аналогов к такому инновационному решению нет, однако __Cloud Voice__ от ВК и __Yandex SpeechKit + YandexDataSphere__ от Яндекса могут частично справиться. Первый подходит для автораспознавания и синтеза речи, имитирующей естесственную. Работает стабильно, ибо у VK есть стабильный и хорошо звучащий голосовой ассистент Маруся. Работает и с потоком аудио, и с отдельными звуковыми файлами, то есть можно использовать и для поддержки, коллцентра, и для озвучки контента. Пара сервисов Яндекса вместе тоже могут приблизиться к закрытию тех же задач, что и Lex: Первый создаёт голосовых ассистентов с реалистичным голосом, распознаванием речи и аналитикой клиентов для автоматизации бизнесов. DataSphere же предоставляет инструменты для всего ML пайплайна: от подготовки данных, виртуального окружения и библиотек до предоставления вычислительных мощностей для обучения моделей. Однако, Amazon Lex работает гораздо более автономно.

-----
__Amazon Route53__ это не просто платформа, которая наибыстрейшим образом преобразует доменные имена в IP-адреса для направления трафика на сайт (проще говоря, DNS-сервис), но и соединяет запросы с инфраструктурой, работающей в AWS, с помощью большого числа типов маршрутизации управляет трафиком, позволяет мониторить, оперативно выявлять сбои и легко настроить ремаршрутизацию. Также, как и остальные сервисы AWS, Route53 безопасна, масштабируема и предоставляет широчайший выбор тарифных планов. Однако, он не поддерживает DNSSEC и не предоставляет опции переадресации для доменов, используемых в локальной сети.

__Yandex Cloud DNS__ так же, как и Route53, не только предоставляет возможность создания доменных зон и оптимизирует запросы, но и даёт возможность для мониторинга и конфигурации через консоль Yandex Cloud, API, CLI и Terraform.

__VK Cloud DNS__ предоставляет приватный и публичный DNS, в остальном функционал скуден, зато настройка очень простая.